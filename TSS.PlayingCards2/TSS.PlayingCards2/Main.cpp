
// Lab Exercise 2 - Playing Cards
// Tami Sweitzer
// 2/23/19


#include <iostream>
#include <conio.h>
#include <string>


enum Suit
{
	HEARTS,
	ACES,
	SPADES,
	DIAMONDS
};

enum Rank
{
	ONE = 1,
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

struct Card
{
	Suit suit;
	Rank rank;
};

// Function prototypes
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);


int main()
{
	Card myCard;
	myCard.rank = EIGHT;
	myCard.suit = SPADES;

	Card otherCard;
	otherCard.rank = TEN;
	otherCard.suit = HEARTS;

	PrintCard(myCard);
	HighCard(myCard, otherCard);

	_getch();
	return 0;
}


// Functions
void PrintCard(Card card)
{
	// prints rank and suit of card
	std::cout << "This is the " << card.rank << " of " << card.suit << std::endl;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		std::cout << "The " << card1.rank << " of " << card1.suit << " is the high card." << std::endl;
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		std::cout << "The " << card2.rank << " of " << card2.suit << " is the high card." << std::endl;
		return card2;
	}
	else if (card1.rank == card2.rank)
	{
		std::cout << "The cards are equal" << std::endl;
		return card1;	// What would this return?
	}
	else
	{
		std::cout << "The high card cannot be determined" << std::endl;
		return card1;
	}

}